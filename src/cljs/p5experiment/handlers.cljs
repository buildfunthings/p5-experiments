(ns p5experiment.handlers
    (:require [re-frame.core :as re-frame]
              [p5experiment.db :as db]))

(re-frame/register-handler
 :set-diameter
 (fn [db [handler diameter]]
   (assoc db :diameter diameter)))

(re-frame/register-handler
 :set-angle
 (fn [db [handler angle]]
   (assoc db :angle angle)))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))
