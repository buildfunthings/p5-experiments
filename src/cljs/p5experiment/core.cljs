(ns p5experiment.core
  (:require [clojure.string :as str]
            [devtools.core :as devtools]
            [p5experiment.config :as config]
            p5experiment.handlers
            p5experiment.subs
            [p5experiment.views :as views]
            [p5experiment.words :refer [words]]
            [re-frame.core :as re-frame]
            [reagent.core :as reagent]))

(def game-state (atom {:words []
                       :buffer ""
                       :score 0
                       :width 100
                       :level 1
                       :hits 0
                       :textwidth 10}))

(def buffer (atom ""))
(def missed (atom 0))

(defn spawn-word [old-words]
  (let [n (rand-nth words)
        y (+ 20  (rand-int  (- js/height 60)))]
    ((comp vec flatten conj) old-words {:text n :x 0 :y y})
    )
  )

(defn setup-game []
  (js/textSize 20)
  (let [tw (js/textWidth "a")]
    (swap! game-state assoc :width (int (/ js/width tw)))
    (swap! game-state assoc :textwidth (int tw)))
  ;;  (js/setInterval spawn-word 1000)
  )

(defn ^:export setup []
  (js/createCanvas 720 400)
  (js/textFont "monospace")
  (setup-game))

(defn draw-hud [world]
  (js/textSize 12)
  (js/text (str "Score: " (:score world)
                " Buffer: [" @buffer "]"
                " Level: " (:level world)
                " score: " (:score world)
                " missed: " @missed
                )
           10 (- js/height 20))
  world)


(defn draw-word [word world]
  (let [w js/width
        curx (:x word)
        newx (+ curx (+ .5 (* .1 (:level world))))
        t (:text word)
        perc (int (* (/ (- w newx) w) 100))]

    (condp < perc
      65 (js/fill 255)
      30 (js/fill 255 255 0)
      (js/fill 255 0 0))

    (js/text t (:x word) (:y word))
    (if (< newx js/width)
      (assoc word :x newx) ;; update the word
      (do (swap! missed inc)
          nil) ;; update the missed count
      )))

(defn draw-words [world]
  (js/textSize 20)
  (assoc world :words (doall (map #(draw-word % world)
                                  (filter #(not (nil? %)) (:words world))))))

(defn ^:export keyTyped [key keyCode]
  (let [current @buffer
        backspace? (= "\b" key)
        clear? (= 13 keyCode)
        adjusted (if backspace?
                   (if (not (empty? current))
                     (subs current 0 (-  (count current) 1))
                     "")
                   (str current (str/lower-case key)))]
    (if clear?
      (reset! buffer "")
      (reset! buffer adjusted))))

(defn score-single [entry]
  (let [w js/width
        left (- w (:x entry))
        perc (- 100 (int (* (/ left w) 100)))]
    perc))

(defn score [matches world]
  (reduce + (map #(score-single %) matches)))

(defn check-words [ world]
  (let [w (:words world)
        b @buffer
        match (filter #(= (:text %) b) w)
        hits (count match)
        score (+ (:score world) (score match world))]

    (when (not (empty? match))
      (reset! buffer ""))

    (-> world
        (assoc :hits (+ (:hits world) hits))
        (assoc :score score)
        (assoc :words (filter #(not (= (:text %) b)) w)))))

(defn spawn-words [ world ]
  (if (or (= 0 (rem js/frameCount 200))
          (< (count (:words world)) (:level world)))
    (assoc world :words (spawn-word (:words world)))
    world
    ))

;;(level-up @game-state)
(defn level-up [world]
  (if (and (> (:hits world) 0)
           (= 0 (rem (:hits world) 10)))
    (-> world
        (assoc :level (inc (:level world)))
        (assoc :hits 0))
    world)
  )

(defn draw-game []
  (let [new-world (-> @game-state
                      (draw-hud)
                      (draw-words)
                      (check-words)
                      (spawn-words)
                      (level-up)
                      )]
    (reset! game-state new-world))
  )

(defn draw-centered-at-height [t height]
  (let [s (js/textWidth t)]
    (js/text t (- (/ js/width 2) (/ s 2)) height))
  )

(defn end-screen [world]
  (draw-centered-at-height "Game Over!" (/ js/height 2))
  (draw-centered-at-height (str "Score: " (:score world) ) (+  (/ js/height 2) 30)))

(defn ^:export draw []
  (js/background 51)
  (js/fill 255)

  (if (< @missed 10)
    (draw-game)
    (end-screen @game-state))
    
  )

(defn dev-setup []
  (when config/debug?
    (println "dev mode")
    (devtools/install!)))

(defn mount-root []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
