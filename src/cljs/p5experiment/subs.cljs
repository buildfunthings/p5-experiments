(ns p5experiment.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))

(re-frame/register-sub
 :angle
 (fn [db]
   (reaction (:angle @db))))

(re-frame/register-sub
 :diameter
 (fn [db]
   (reaction (:diameter @db))))

(re-frame/register-sub
 :name
 (fn [db]
   (reaction (:name @db))))
