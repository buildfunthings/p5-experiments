function setup() {
    p5experiment.core.setup();
}

function draw() {
    p5experiment.core.draw();    
}

function keyPressed() {
    if (keyCode !== 91) {
	p5experiment.core.keyTyped(key, keyCode);
    }
}
